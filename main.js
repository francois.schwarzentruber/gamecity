$().ready(function () {
	update();
	$("input").click(update);
});


function update() {
	$("#info div, #parameters div").each(function () {
		let classList = $(this).attr('class').split(/\s+/);
		let visible = true;
		for (let c of classList) {
			if (!c.startsWith("!"))
			if (!$(document).find('input[value="' + c + '"]').is(':checked'))
				visible = false;

			if (c.startsWith("!"))
				if ($(document).find('input[value="' + c.substr(1) + '"]').is(':checked'))
					visible = false;
		}

		if (visible)
			$(this).show();
		else
			$(this).hide();

	});
}
